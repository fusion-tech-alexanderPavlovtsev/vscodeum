import path from "path";
import { Command } from "commander";
import { serve } from "@jsnote-jbook/local-api";
import chalk from "chalk";

const link = chalk.bold.blue.underline;

const isProduction = process.env.NODE_ENV === "production";

export const serveCommand = new Command()
  .command("serve [filename]")
  .description("Open a file for editting")
  .option("-p, --port <number>", "port to run server on", "4005")
  .action(async (filename = "notebook.js", options: { port: string }) => {
    try {
      const dir = path.join(process.cwd(), path.dirname(filename));
      await serve(
        parseInt(options.port),
        path.basename(filename),
        dir,
        !isProduction
      );
      console.log(
        `Opened ${filename}. Navigate to ${link(
          `http://localhost:${options.port}`
        )} to edit the notes`
      );
    } catch (err) {
      if (err instanceof Error) {
        let error = err as NodeJS.ErrnoException;
        if (error.code === "EADDRINUSE") {
          console.error("Port is in use. Try running on a different port");
        } else {
          console.log("Heres the problem", error.message);
        }
        process.exit(1);
      }
    }
  });
