import { TypedUseSelectorHook } from "react-redux";
import { useSelector as useSelectorFunc } from "react-redux";

import { RootState } from "../state/store";

export const useSelector: TypedUseSelectorHook<RootState> = useSelectorFunc;
