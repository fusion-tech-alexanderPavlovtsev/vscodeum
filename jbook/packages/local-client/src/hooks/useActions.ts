import { useMemo } from "react";
import { bindActionCreators } from "redux";
import { useDispatch } from "react-redux";

import ActionCreators from "../state/action-creators/index";

export const useActions = () => {
  const dispatch = useDispatch();
  return useMemo(
    () => bindActionCreators(ActionCreators, dispatch),
    [dispatch]
  );
};
