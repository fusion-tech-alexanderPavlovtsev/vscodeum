import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { customAlphabet } from "nanoid";
import axios from "axios";

import { RootState, SliceActions } from "./../../store";
import { Cell } from "../../cell";
import {
  CellMovePayloadType,
  CellState,
  InsertCellAfterPayloadType,
  UpdateCellPayloadType,
} from "./types";

const nanoid = customAlphabet("AaBbCcDdEeFfJjGgKkOoPpQqWwXxYyZz1234567890", 4);

export const fetchCells = createAsyncThunk<
  Cell[] | undefined,
  void,
  {
    rejectValue: string;
  }
>("cells/fetchCells", async (args, { rejectWithValue }) => {
  try {
    const { data }: { data: Cell[] } = await axios.get("/cells");
    return data;
  } catch (error) {
    if (error instanceof Error) {
      return rejectWithValue(error.message);
    }
  }
});

export const saveCells = createAsyncThunk<
  void,
  void,
  {
    rejectValue: string;
  }
>("cells/saveCells", async (args, { rejectWithValue, getState }) => {
  const {
    cells: { data, order },
  } = getState() as RootState;
  const cells = order.map((id) => data[id]);
  try {
    await axios.post("/cells", { cells });
  } catch (error) {
    if (error instanceof Error) {
      return rejectWithValue(error.message);
    }
  }
});

export const initialState: CellState = {
  loading: false,
  error: null,
  order: [],
  data: {},
};

export const cellsSlice = createSlice({
  name: "cells",
  initialState,
  reducers: {
    updateCell(state: CellState, action: PayloadAction<UpdateCellPayloadType>) {
      const { id, content } = action.payload;
      state.data[id].content = content;
    },
    deleteCell(state: CellState, action: PayloadAction<string>) {
      delete state.data[action.payload];
      state.order = state.order.filter((id) => id !== action.payload);
    },
    moveCell(state: CellState, action: PayloadAction<CellMovePayloadType>) {
      const { id, direction } = action.payload;
      const index = state.order.findIndex((elementId) => elementId === id);
      const targetIndex = direction === "up" ? index - 1 : index + 1;
      // Check if new target index is legal
      if (targetIndex < 0 || targetIndex > state.order.length - 1) {
        return;
      }
      state.order[index] = state.order[targetIndex];
      state.order[targetIndex] = action.payload.id;
    },
    insertCellAfter(
      state: CellState,
      action: PayloadAction<InsertCellAfterPayloadType>
    ) {
      const cell: Cell = {
        id: nanoid(),
        content: "",
        type: action.payload.type,
      };
      state.data[cell.id] = cell;
      const foundIndex = state.order.findIndex(
        (id) => id === action.payload.id
      );
      if (foundIndex < 0) {
        state.order.unshift(cell.id);
      } else {
        state.order.splice(foundIndex + 1, 0, cell.id);
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCells.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchCells.fulfilled, (state, action) => {
        state.error = null;
        if (action.payload) {
          state.order = action.payload.map((cell) => cell.id);
          state.data = action.payload.reduce((acc, cell) => {
            acc[cell.id] = cell;
            return acc;
          }, {} as CellState["data"]);
        }
      })
      .addCase(fetchCells.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      .addCase(saveCells.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      });
  },
});

export const { actions } = cellsSlice;

export type CellsActionTypes = SliceActions<typeof cellsSlice.actions>;

export default cellsSlice.reducer;
