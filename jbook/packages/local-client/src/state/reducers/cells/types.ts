import { Cell, CellTypes } from "./../../cell";

export interface CellState {
  loading: boolean;
  error: string | null;
  order: string[];
  data: {
    [key: string]: Cell;
  };
}

export type CellMovePayloadType = {
  id: string;
  direction: Direction;
};

export type InsertCellAfterPayloadType = {
  id: string | null;
  type: CellTypes;
};

export type UpdateCellPayloadType = {
  id: string;
  content: string;
};

type Direction = "up" | "down";
