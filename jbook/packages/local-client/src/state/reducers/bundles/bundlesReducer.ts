import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import bundle from "../../../bundler";
import { BundlesState } from "./types";

export const createBundle = createAsyncThunk(
  "bundles/createBundle",
  async (
    { cellId, input }: { cellId: string; input: string },
    { rejectWithValue }
  ) => {
    try {
      const result = await bundle(input);
      return {
        cellId,
        bundle: result,
      };
    } catch (error) {
      if (error instanceof Error) return rejectWithValue(error.message);
    }
  }
);

export const initialState: BundlesState = {};

export const bundlesSlice = createSlice({
  name: "bundles",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(createBundle.pending, (state, action) => {
        if (action.meta.arg) {
          const { cellId } = action.meta.arg;
          state[cellId] = {
            loading: true,
            code: "",
            err: "",
          };
        }
      })
      .addCase(createBundle.fulfilled, (state, action) => {
        if (action.payload) {
          state[action.payload.cellId] = {
            loading: false,
            code: action.payload.bundle.code,
            err: action.payload.bundle.err,
          };
        }
      });
  },
});

export const { actions } = bundlesSlice;

export default bundlesSlice.reducer;
