export interface BundlesState {
  [key: string]:
    | {
        loading: boolean;
        code: string;
        err: string;
      }
    | undefined;
}

export type BundleType = {
  code: string;
  err: string;
};
export interface BundleCompletePayloadType {
  cellId: string;
  bundle: BundleType;
}
