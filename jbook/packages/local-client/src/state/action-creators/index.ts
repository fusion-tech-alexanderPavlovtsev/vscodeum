/* eslint-disable import/no-anonymous-default-export */
import { actions as cellsActions } from "../reducers/cells/cellsReducer";
import { actions as bundlesActions } from "../reducers/bundles/bundlesReducer";

export default {
  ...cellsActions,
  ...bundlesActions,
};
