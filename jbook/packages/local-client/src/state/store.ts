import { persistMiddleware } from "./middlewares/persist-middleware";
import { configureStore, combineReducers } from "@reduxjs/toolkit";

import bundlesReducer from "./reducers/bundles/bundlesReducer";
import cellsReducer from "./reducers/cells/cellsReducer";

const rootReducer = combineReducers({
  cells: cellsReducer,
  bundles: bundlesReducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(persistMiddleware),
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];

export type SliceActions<T> = {
  [K in keyof T]: T[K] extends (...args: any[]) => infer A ? A : never;
}[keyof T];

const store = setupStore();

export default store;
