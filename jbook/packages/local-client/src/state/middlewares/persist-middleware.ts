import { Dispatch } from "redux";

import { CellsActionTypes } from "../reducers/cells/cellsReducer";
import { actions } from "../reducers/cells/cellsReducer";
import { RootState } from "./../store";
import { saveCells } from "./../reducers/cells/cellsReducer";

export const persistMiddleware = ({
  dispatch,
  getState,
}: {
  dispatch: Dispatch<CellsActionTypes>;
  getState: () => RootState;
}) => {
  let timer: any;
  return (next: (action: CellsActionTypes) => void) => {
    return (action: CellsActionTypes) => {
      next(action);
      if (
        [
          actions.moveCell.type,
          actions.updateCell.type,
          actions.insertCellAfter.type,
          actions.deleteCell.type,
        ].includes(action.type)
      ) {
        if (timer) {
          clearTimeout(timer);
        }
        timer = setTimeout(() => {
          saveCells()(dispatch, getState, null);
        }, 250);
      }
    };
  };
};
