import { FC } from "react";

interface ActionButtonProps {
  handler: () => void;
  icon: string;
}

const ActionButton: FC<ActionButtonProps> = ({ handler, icon }) => {
  return (
    <button className="button is-primary is-small" onClick={handler}>
      <span className="icon">
        <i className={`fas ${icon}`} />
      </span>
    </button>
  );
};

export default ActionButton;
