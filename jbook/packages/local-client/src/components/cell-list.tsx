import { FC, Fragment, useMemo, useEffect } from "react";

import { useSelector } from "../hooks/useTypedSelector";
import CellListItem from "./cell-list-item";
import AddCell from "./add-cell";
import "./cell-list.css";
import { fetchCells, saveCells } from "../state/reducers/cells/cellsReducer";
import { useDispatch } from "react-redux";

const CellList: FC<any> = () => {
  const cells = useSelector(({ cells: { order, data } }) =>
    order.map((id) => data[id])
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCells());
  }, [dispatch]);

  const renderedCells = useMemo(() => {
    return cells.map((cell) => (
      <Fragment key={cell.id}>
        <CellListItem cell={cell} />
        <AddCell prevCellId={cell.id} />
      </Fragment>
    ));
  }, [cells]);

  return (
    <div className="cell-list">
      <AddCell forceVisible={cells.length === 0} prevCellId={null} />
      {renderedCells}
    </div>
  );
};

export default CellList;
