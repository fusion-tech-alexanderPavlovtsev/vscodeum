import MDEditor from "@uiw/react-md-editor";
import { useState, useRef, FC } from "react";

import useClickOutside from "../hooks/useClickOutside";
import { Cell } from "../state/cell";
import "./text-editor.css";
import { useActions } from "../hooks/useActions";

interface TextEditorProps {
  cell: Cell;
}

const TextEditor: FC<TextEditorProps> = ({ cell: { id, content } }) => {
  const ref = useRef<HTMLDivElement | null>(null);
  const { updateCell } = useActions();
  const [editing, setEditing] = useState(false);
  useClickOutside(ref, () => setEditing(false));

  if (editing) {
    return (
      <div className="text-editor" ref={ref}>
        <MDEditor
          value={content}
          onChange={(v) => updateCell({ id, content: v || "" })}
        />
      </div>
    );
  }

  return (
    <div className="text-editor card" onClick={() => setEditing(true)}>
      <div className="card-content">
        <MDEditor.Markdown source={content || "Click to edit"} />
      </div>
    </div>
  );
};

export default TextEditor;
