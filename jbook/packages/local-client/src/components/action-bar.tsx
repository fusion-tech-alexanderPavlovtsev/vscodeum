import { FC } from "react";

import { useActions } from "../hooks/useActions";
import ActionButton from "./action-button";
import "./action-bar.css";

interface ActionBarProps {
  id: string;
}

const ActionBar: FC<ActionBarProps> = ({ id }) => {
  const { moveCell, deleteCell } = useActions();
  return (
    <div className="action-bar">
      <ActionButton
        icon="fa-arrow-up"
        handler={() => moveCell({ id, direction: "up" })}
      />
      <ActionButton
        icon="fa-arrow-down"
        handler={() => moveCell({ id, direction: "down" })}
      />
      <ActionButton icon="fa-times" handler={() => deleteCell(id)} />
    </div>
  );
};

export default ActionBar;
